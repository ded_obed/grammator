#!/usr/bin/env python3

import unittest
import gror

class TestTerminal(unittest.TestCase):
    def setUp(self):
        pass

    def test_existence(self):
        term = gror.Term("Ahoj")

    def test_inequality(self):
        term1 = gror.Term("Ahoj")
        term2 = gror.Term("Nazdar")
        self.assertNotEqual(term1, term2)

    def test_equality(self):
        term1 = gror.Term("Ahoj")
        term2 = gror.Term("Ahoj")
        self.assertEqual(term1, term2)

if __name__ == "__main__":
    unittest.main(verbosity=2)

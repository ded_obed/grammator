#! /usr/bin/env python3

# Project: Grammar Translator
# Author: Karel Benes, xbenes20@stud.fit.vutbr.cz
# Purpose: Translate grammars between the W3C SRGS and Kaldi nets
#   and possibly other formats (STK?).


# A grammar class.
# In general a CFG is allowed, though you will only want a (right) linear one
# most of the time.
# Grammar knows all its terminals and nonterminals and the starting symbol.
# Grammar is able to spit out its FST equivalent.
class GrammarNotReady(ValueError):
    pass

class Grammar:
    def __init__(self):
        self.starter = None
        self.terms = {}
        self.nonterms = {}

    def add_starting(self, starter):
        if not type(starter) == str:
            raise TypeError("Starter has to be specified by a name")
        self.starter = starter

    def add_term(self, terminal):
        if not isinstance(terminal,Term):
            raise TypeError("Term has to be an instance of Term")
        self.terms[terminal.word] = terminal

    def add_nonterm(self, nonterminal):
        if not isinstance(nonterminal,NonTerm):
            raise TypeError("Nonterminal has to be an instance of NonTerm")
        self.nonterms[nonterminal.label] = nonterminal

    def complete(self):
        if not self.starter:
            return False

        if self.starter not in self.nonterms:
            return False

        for nonterm in self.nonterms.values():
            for rhs in nonterm.rules:
                for elem in rhs:
                    if elem not in self.terms and elem not in self.nonterms:
                        return False
                    
        return True

    def right_linear(self):
        """
        If you ask a noncomplete grammar about its left linearity, a tyrannousaurus
        may come to bite you.
        """
        for nonterm in self.nonterms.values():
            for rhs in nonterm.rules:
                for elem in rhs[:-1]:
                    if elem not in self.terms:
                        return False
            
        return True

    def fstize(self):
        raise GrammarNotReady("I'm too shy")

# A nonterminal element.
# It knows what it can expand into, each rule's RHS is a list of references
# to (non)terminals.
# Upon creation is empty and supports rule addition.
class WrongRHS(Exception):
    pass

class NonTerm:
    def __init__(self, label):
        self.label = label
        self.rules = []

    def add_rule(self, rhs):
        if type(rhs) != list:
            raise WrongRHS("RHS of a rule has to be a list!")
        for elem in rhs:
            if not isinstance(elem, str):
                raise WrongRHS("RHS has to consist names only")

        self.rules.append(rhs)


# A terminal element.
# Whatever it is, it is the same when it looks the same (same word or phn),
# therefore a string identifies it enough. We only have a class for it
# in order to force Python into some type controls.
# Is only created, compared and passed to writer.
class Term:
    def __init__(self, word):
        self.word = word

    def __eq__(self, other):
        return self.word == other.word

# A general (nondeterministic with epsilon rules) FST.
# Knows to be printed for OpenFST. # and in general turned back into grammar
# Has always one starting and exactly one ending state.
# From maths:
#   Input alphabet  -- unnecessary
#   Output alphabet -- unnecessary
#   States          -- labeled by numbers - implicitly by index
#   Starter         -- number - zero
#   Final           -- number - last one
#   Arcs            -- hidden in states
import sys

class GeneralFST:
    def _fstize_nonterm(self,nonterm,predecessor):
        # make the ender
        self.content.append([])
        ender_index = len(self.content)-1

        # for each rhs:
        for rhs in nonterm.rules:
        #   prepare first state
            self.content.append([(0,"","",None)])
        #   tell the predecessor it can eps-move into it
            predecessor.append((len(self.content)-1,"","",None))
        
        #   for each term in rhs:
            for elem in rhs:
        #       prepare new state
                self.content.append([0,"","",None])
        #       tell the previous to term-move into the new one
                self.content[-2] = ([(len(self.content)-1,elem,elem,None)])
        #   tell the last state to eps-move into the ender
            self.content[-1] = ([(ender_index,"","",None)])

    def __init__(self, grammar):
        if not isinstance(grammar, Grammar):
            raise TypeError("A GeneralFST instance may only be built upon a Grammar")

        self.content = [[]]
        self._fstize_nonterm(grammar.nonterms[grammar.starter], self.content[0])

# A state of GFST.
# List of arcs.

# An arc of GFST.
# Tuple (Destination, Input, Output, Weight)

# An XML reader.

# An XML writer.


# What happens:
# 1. a proper format reader is created over the input stream (is stdin enough?)
# 2. the reader parses the input and creates the grammar
# 3. the grammar turns itself into (an equivalent) FST
# 4. a proper format writer is created over the output stream (is stdout enough?)
# 5. the writer is given the grammar and puts it down


if __name__ == "__main__":
    print("Hello world.")

#!/usr/bin/env python3

import unittest
import gror

class TestGFST(unittest.TestCase):
    def setUp(self):
        pass

    def test_existence(self):
        # grammar of S->s only.
        grammar = gror.Grammar()
        grammar.add_starting("S")
        starter = gror.NonTerm("S")
        starter.add_rule(["s"])
        term = gror.Term("s")
        grammar.add_term(term)
        grammar.add_nonterm(starter)
        gfst = gror.GeneralFST(grammar)

    def test_only_grammar_for_constructor(self):
        self.assertRaises(TypeError, gror.GeneralFST, 1)

if __name__ == "__main__":
    unittest.main(verbosity=2)

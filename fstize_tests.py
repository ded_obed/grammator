#!/usr/bin/env python3

import unittest
import gror

class TestGrammar2FST(unittest.TestCase):
    def setUp(self):
        pass

    def test_existence(self):
        self.assertRaises(gror.GrammarNotReady,gror.Grammar().fstize())
    
    def test_single_term(self):
        # grammar of S->s only.
        grammar = gror.Grammar()
        grammar.add_starting("S")
        starter = gror.NonTerm("S")
        starter.add_rule(["s"])
        term = gror.Term("s")
        grammar.add_term(term)
        grammar.add_nonterm(starter)
        gfst = gror.GeneralFST(grammar)
        self.assertEqual(gfst.content,[
            [(2,"","",None)],    # 0
            [],                 # 1
            [(3,"s","s",None)], # 2
            [(1,"","",None)]    # 3
            ])

    def test_single_string(self):
        # grammar of S->abc only.
        grammar = gror.Grammar()
        grammar.add_starting("S")
        starter = gror.NonTerm("S")
        starter.add_rule(["a","b","c"])
        a = gror.Term("a")
        b = gror.Term("b")
        c = gror.Term("c")
        grammar.add_term(a)
        grammar.add_term(b)
        grammar.add_term(b)
        grammar.add_nonterm(starter)
        gfst = gror.GeneralFST(grammar)
        self.assertEqual(gfst.content,[
            [(2,"","",None)],   # 0
            [],                 # 1
            [(3,"a","a",None)], # 2
            [(4,"b","b",None)], # 3
            [(5,"c","c",None)], # 4
            [(1,"","",None)]
            ])

    def test_alternatives(self):
        # grammar of S->a, S->b.
        grammar = gror.Grammar()
        grammar.add_starting("S")
        starter = gror.NonTerm("S")
        starter.add_rule(["a"])
        starter.add_rule(["b"])
        a = gror.Term("a")
        b = gror.Term("b")
        grammar.add_term(a)
        grammar.add_term(b)
        grammar.add_nonterm(starter)
        gfst = gror.GeneralFST(grammar)
        import sys
        sys.stderr.write("\n"+str(gfst.content)+"\n")
        self.assertEqual([
            [(2,"","",None), (4,"","",None)],   # 0
            [],                                 # 1
            [(3,"a","a",None)],                 # 2
            [(1,"","",None)],                   # 3
            [(5,"b","b",None)],                 # 4
            [(1,"","",None)],                   # 5
            ],
            gfst.content)

if __name__ == "__main__":
    unittest.main(verbosity=2)

#!/usr/bin/env python3

import unittest
import gror

class TestGrammar(unittest.TestCase):
    def setUp(self):
        pass

    def test_existence(self):
        grammar = gror.Grammar()

#--------------

    def test_starter_addition(self):
        grammar = gror.Grammar()
        nonterm = gror.NonTerm("A")
        grammar.add_starting("A")

    def test_starter_type(self):
        grammar = gror.Grammar()
        self.assertRaises(TypeError,grammar.add_starting,1)

#---------------

    def test_term_addition(self):
        grammar = gror.Grammar()
        term = gror.Term("a")
        grammar.add_term(term)

    def test_term_type(self):
        grammar = gror.Grammar()
        self.assertRaises(TypeError,grammar.add_term,"a")

#---------------

    def test_nonterm_addition(self):
        grammar = gror.Grammar()
        nonterm = gror.NonTerm("A")
        grammar.add_nonterm(nonterm)

    def test_nonterm_type(self):
        grammar = gror.Grammar()
        self.assertRaises(TypeError,grammar.add_nonterm,"A")

#----------------

    def test_completness_check_existence(self):
        grammar = gror.Grammar()
        grammar.add_starting("S")
        self.assertFalse(grammar.complete())

    def test_completness_check_simple(self):
        # grammar of S->s only.
        grammar = gror.Grammar()
        grammar.add_starting("S")
        starter = gror.NonTerm("S")
        starter.add_rule(["s"])
        term = gror.Term("s")
        grammar.add_term(term)
        grammar.add_nonterm(starter)
        self.assertTrue(grammar.complete())

    def test_completness_check_missing_term(self):
        # grammar of S->s only.
        grammar = gror.Grammar()
        grammar.add_starting("S")
        starter = gror.NonTerm("S")
        starter.add_rule(["s"])
        grammar.add_nonterm(starter)
        self.assertFalse(grammar.complete())

    def test_completness_check_recursion(self):
        grammar = gror.Grammar()
        grammar.add_starting("S")
        starter = gror.NonTerm("S")
        starter.add_rule(["s"])
        starter.add_rule(["s","S"])
        term = gror.Term("s")
        grammar.add_term(term)
        grammar.add_nonterm(starter)
        self.assertTrue(grammar.complete())

    def test_completness_check_more_nonterms(self):
        # grammar of S->ss, S->sA, A->sSs
        grammar = gror.Grammar()
        grammar.add_starting("S")
        starter = gror.NonTerm("S")
        starter.add_rule(["s","s"])
        starter.add_rule(["s","A"])
        the_a = gror.NonTerm("A")
        the_a.add_rule(["s","S","s"])
        term = gror.Term("s")
        grammar.add_term(term)
        grammar.add_nonterm(starter)
        grammar.add_nonterm(the_a)
        self.assertTrue(grammar.complete())
    
    def test_completness_check_missing_nonterm(self):
        # grammar of S->ss, S->sA, A->sSs
        grammar = gror.Grammar()
        grammar.add_starting("S")
        starter = gror.NonTerm("S")
        starter.add_rule(["s","s"])
        starter.add_rule(["s","A"])
        the_a = gror.NonTerm("A")
        the_a.add_rule(["s","S","s"])
        term = gror.Term("s")
        grammar.add_term(term)
        grammar.add_nonterm(starter)
        self.assertFalse(grammar.complete())

    def test_rlinearity_check_existence(self):
        grammar = gror.Grammar()
        grammar.add_starting("S")
        starter = gror.NonTerm("S")
        starter.add_rule(["s"])
        starter.add_rule(["s","S"])
        term = gror.Term("s")
        grammar.add_term(term)
        grammar.add_nonterm(starter)
        self.assertTrue(grammar.right_linear())
        
    def test_rlinearity_check_more_nonterms(self):
        grammar = gror.Grammar()
        grammar.add_starting("S")
        starter = gror.NonTerm("S")
        starter.add_rule(["s"])
        starter.add_rule(["s","S","S"])
        term = gror.Term("s")
        grammar.add_term(term)
        grammar.add_nonterm(starter)
        self.assertFalse(grammar.right_linear())
        


if __name__ == "__main__":
    unittest.main(verbosity=2)


.SILENT:

# where there is nosetests and rednose ready (such as @home :-) )
testc: *_tests.py gror.py
	nosetests --rednose *_tests.py

# where those are not available (such as @merlin :-( )
test: *_tests.py gror.py
	for test_file in `ls *_tests.py`; \
	do \
		echo ""; \
		echo "+++ $$test_file +++"; \
		python3 $$test_file; \
	done

.PHONY: test testc

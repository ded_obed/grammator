#!/usr/bin/env python3

import unittest
import gror

class TestNonTerminal(unittest.TestCase):
    def setUp(self):
        pass

    def test_existence(self):
        nonterm = gror.NonTerm("A")

    def test_rule_addition_existence(self):
        nonterm = gror.NonTerm("A")
        nonterm.add_rule(["A"])

    def test_rule_has_to_be_list(self):
        nonterm = gror.NonTerm("A")
        self.assertRaises(gror.WrongRHS,nonterm.add_rule,nonterm)

    def test_rule_consists_of_names(self):
        nonterm = gror.NonTerm("A")
        self.assertRaises(gror.WrongRHS,nonterm.add_rule,["A",nonterm])

if __name__ == "__main__":
    unittest.main(verbosity=2)
